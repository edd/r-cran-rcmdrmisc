r-cran-rcmdrmisc (2.9-1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 27 Sep 2023 07:13:19 -0500

r-cran-rcmdrmisc (2.9-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 24 Aug 2023 22:16:34 -0500

r-cran-rcmdrmisc (2.7-2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 06 Jan 2022 17:52:32 -0600

r-cran-rcmdrmisc (2.7-1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 18 Aug 2020 20:49:40 -0500

r-cran-rcmdrmisc (2.7-0-3) unstable; urgency=medium

  * Rebuilt for r-4.0 transition

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 20 May 2020 14:04:10 -0500

r-cran-rcmdrmisc (2.7-0-2) unstable; urgency=medium

  * Source-only upload

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 19 Jan 2020 17:31:17 -0600

r-cran-rcmdrmisc (2.7-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 15 Jan 2020 08:57:19 -0600

r-cran-rcmdrmisc (2.5-1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 12 Sep 2018 19:30:43 -0500

r-cran-rcmdrmisc (2.5-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 23 Aug 2018 07:16:41 -0500

r-cran-rcmdrmisc (1.0-10-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Increased Build-Depends: to 'r-cran-car (>= 3.0-0)'
    [ which lead to a full two-month wait since the release of this version
    in early April as several packages had to get through the NEW queue ]

  * Rebuilding for R 3.5.0 transition
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/compat: Increase level to 9
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 07 Jun 2018 19:01:13 -0500

r-cran-rcmdrmisc (1.0-9-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 20 Mar 2018 20:38:15 -0500

r-cran-rcmdrmisc (1.0-8-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 01 Mar 2018 16:33:00 -0600

r-cran-rcmdrmisc (1.0-7-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 06 Jan 2018 07:56:34 -0600

r-cran-rcmdrmisc (1.0-6-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Added (Build-)Depends on packages r-cran-readstata13,
    r-cran-haven (which also required r-cran-forcats and r-cran-readr) and
    r-cran-nortest, all of which took a few weeks to get into Debian

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/compat: Increase level to 9

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 06 Nov 2017 11:29:47 -0600

r-cran-rcmdrmisc (1.0-5-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 15 Aug 2016 09:50:50 -0500

r-cran-rcmdrmisc (1.0-4-2) unstable; urgency=medium

  * debian/compat: Created			(Closes: #829353)
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 10 Jul 2016 07:21:41 -0500

r-cran-rcmdrmisc (1.0-4-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 04 Apr 2016 07:37:19 -0500

r-cran-rcmdrmisc (1.0-3-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Add new (Build-)Depends: r-cran-readxl

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 05 Aug 2015 14:41:52 -0500

r-cran-rcmdrmisc (1.0-2-2) unstable; urgency=low

  * debian/control: Added (Build-)Depends: on r-cran-foreign, r-cran-nnet
  						      	(#Closes: 763247)
  
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 28 Sep 2014 13:45:23 -0500

r-cran-rcmdrmisc (1.0-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 05 Sep 2014 07:30:51 -0500

r-cran-rcmdrmisc (1.0-1-1) unstable; urgency=low

  * Initial Debian release 				(Closes: #759587)

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 28 Aug 2014 15:52:11 -0500


